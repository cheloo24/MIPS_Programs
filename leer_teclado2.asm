.data

# mostramos hola cada vez que leemos un A
coord: .asciiz "Ingrese un n�mero para coordenadas del tablero: \n Digite 1 si quiere un tablero de 4x4\n Digite 2 si quiere un tablero de 10x10\n Digite 3 si quiere un tablero de 16x16"
enter: .asciiz "\n"

.text

menu: 
addi $v0, $zero, 4
la $a0, coord
syscall

addi $v0, $zero, 4
la $a0, enter
syscall
# Polling del teclado. Vamos preguntando cada 100ms si el estado del teclado es
# 'A' u otro valor
polling:
addi $a0, $zero, 100	# cada 100 ms
addi $v0, $zero, 32     # esperamos (syscall 32 sleep)
syscall			        # esperamos 100ms para no utilizar todo el procesador

addi $t0, $zero, 0xffff0004		# los caracteres del teclado se guardan en la dir de mem 0xffff0004
lw $t1, 0($t0)
sw $zero, 0($t0)	        # Seteamos a NULL (cero) el buffer para no leer datos duplicados
beq $t1, 49, uno 			# n�mero 1 es 49 en ascii
beq $t1,50,dos				# n�mero 2 es 50 en ascii
beq $t1,51, tres			# n�mero 3 es 51 en ascii
j polling


# mostramos la cadena en salida y volvemos a hacer polling del teclado
uno: 
# aqui va lo que pasa en caso de un tablero de 4x4

dos:
# aqui va lo que pasa en caso de un tablero de 10x10

tres:
# aqui va lo que pasa en caso de un tablero de 16x16

fin:
