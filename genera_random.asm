.data
tablero: .space 300
.text
la $s1,tablero
addi $a0,$zero,4 #cantidad de filas y columnas
GENERA_RANDOM:
	add $s0,$a0,$zero #copio el numero de filas y col en s0
	mul $s0,$s0,$s0 #guardo en s0 el numero de posiciones posibles para las bombas
	addi $t1,$zero, -1
	addi $t2, $zero, 0 #contador de bombas
	NUMERO_DE_BOMBAS:
		div $a1, $s0, 4 #numero de bombas va a ser igual a cuadrados del tablero/4
		move $s2, $a1 #pongo el numero de bombas en s2
	RANDOM_POSICION:
	li $a0,4 # cualquier entero para a0
	add $a1, $s0,$zero #limite superior del rango para generar al azar
	li $v0,42 # random range int
	syscall
	move $t0, $a0 #dejo en t0 el numero random generado
	mul $t3,$t0,4 #guardo en t3 la posicion del arreglo es decir posicion*4
	add $s1,$s1,$t3
	sw $t1,($s1) #pongo la bomba en la posicion entregada por el random
	addi $t2, $t2, 1 #incremento contador de bombas
	bne $t2,$s2,RANDOM_POSICION
	move $v0, $s1 #retorna el tablero lleno por random
	move $v1, $t2 #retorna la cantidad de bombas
	
	
	
	
