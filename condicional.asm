# Implementar
# f, g, h, i, j en registros $s0, $s4
#
# if( i == j)
# {
#	f = g + h;
# }else{
#	f = g - h;
# }
#

.data

f: .word
g: .word
h: .word
i: .word
j: .word

.text

la $t1, f               # Cargar dirección de memoria de f
lw $s0, 0($t1)          # Cargar la palabra apuntada por la dirección de memoria de $t1

la $t1, g
lw $s1, 0($t1)

la $t1, h
lw $s2, 0($t1)

la $t1, i
lw $s3, 0($t1)

la $t1, j
lw $s4, 0($t1)


bne $s3, $s4, ELSE     # si, i == j
add $s0, $s1, $s2      # sumar g + h
j FIN_ELSE             # ir al final del if
ELSE:
sub $s0, $s1, $s2      # restar g - h
FIN_ELSE:
