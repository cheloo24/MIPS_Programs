# Convertir desde C
#
# int f, g, h, i, j;
#
# int main()
# {
# 	f = (g + h) - (i + j);
# }
#

.data

f: .word 
g: .word 10
h: .word 20
i: .word 30
j: .word 40


.text


la $t1, g
lw $t2, 0($t1)		# Se carga el valor apuntado por la dirección de memoria de la etiqueta g

la $t1, h
lw $t3, 0($t1)

add $t5, $t2, $t3	# Se suma el valor de la etiqueta g con h

la $t1, i
lw $t2, 0($t1)

la $t1, j
lw $t3, 0($t1)

add $t6, $t2, $t3	# Se suma el valor de la etiqueta i con j


sub $t2, $t5, $t6	

la $t1, f               # cargar la dirección de memoria de la etiqueta f
sw $t2, 0($t1)          # guardar el valor de $t2 en la dirección de memoria de la etiqueta f
