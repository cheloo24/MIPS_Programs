# implementar un bucle while
# Variables i, j, k desde $s3 a $s5
# base arreglo es $s6
#
# while(save[i] == k)
# {
# 	i++;
# }
#
# Este ejemplo debería dar como resultado en $s3 => 3, la cantidad de unos

.data

save: .word 1, 1, 1, 3


.text

addi $s3, $zero, 0 	# i parte en 0
add $s4, $zero, $zero   # j va a partir en 0
addi $s5, $zero, 1      # k es 1, para contar la cantidad de unos

la $s6, save		# la base del array


WHILE:
sll $t2, $s3, 2		# i*2^2
add $t1, $s6, $t2       # (base + i*4)
lw $t0, 0($t1)		# cargamos la palabra en el array
bne $t0, $s5, FIN_WHILE # si no es igual terminamos
addi $s3, $s3, 1 	# si es igual incrementamos i++
j WHILE
FIN_WHILE:

