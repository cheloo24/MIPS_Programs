.data

buffer: .byte 0:100
archivo: .asciiz "prueba.txt"
msg_error: .asciiz "Error :("

.text
	
#$a0 = address of null-terminated string containing filename
#$a1 = flags
#$a2 = mode



la $a0, archivo
addi $a1, $zero, 0 	# 0 => read, 1 => write-create, 9 => write-append-create
addi $a2, $zero, 0      # Siempre es ignorado por mars
li $v0, 13		# Syscall 13 lee de un archivo
syscall
move $s0, $v0


WHILE:
move $a0, $s0
la $a1, buffer
li $a2, 99		# 99 ya que hay que incluir el terminador en null
li $v0, 14		# syscall 14 lee de un archivo
syscall
beq $v0, 0, FIN
blt $v0, 0, ERROR

li $v0, 4
la $a0, buffer
syscall
j WHILE

ERROR:

li $v0, 4
la $a0, msg_error
syscall

FIN:
