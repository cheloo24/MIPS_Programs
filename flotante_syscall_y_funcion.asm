.data

pi: .float 3.141592653
pregunta: .asciiz "Radio circunferencia: "


.text


la $a0, pregunta		
addi $v0, $zero, 4
syscall			# muestra el string "pregunta" con la syscall 4

addi $v0, $zero, 5	
syscall			# pregunta por un entero al usuario usando la syscall 5

add $a0, $zero, $v0	# mueve el entero entregado por la syscall 5 a a0, el argumento de la funcion area
jal area		# llama a la funcion area



# En esta seccion debemos pasar el valor de v0 al coprocesador1 ya que
# el flotante a mostrar debe estar en f12
#
addi $sp, $sp, -4	# espacio para una palabra en el stack
sw $v0, 0($sp)		# movemos el valor devuelto por la funcion area al stack
l.s $f12, 0($sp)	# cargamos la palabra del stack al coprocesador1
addi $sp, $sp, 4	# restauramos el stack en la posicion original
addi $v0, $zero, 2	# vamos a usar la syscall 2 para mostrar el flotante en f12
syscall			# mostramos el flotante



addi $v0, $zero, 10	
syscall			# usamos la syscall 10 para salir del programa (exit(0))




# Calcula A = pi*r^2
# Devuvelve $v0 en formato flotante
#
area:
addi $sp, $sp, -4 		# espacio en el stack para pasar datos al cop1
sw $a0, 0($sp)			# guardamos el argumento en el stack
l.s $f1, pi			# cargamos pi
l.s $f0, 0($sp)			# cargamos el argumento en cop1
cvt.s.w $f0, $f0		# convertimos el entero en flotante
mul.s $f0, $f0, $f0  		# r^2	
mul.s $f0, $f0, $f1  		# pi*r^2
s.s $f0, 0($sp)			# guardamos el area en el stack
lw $v0, 0($sp)			# cargamos el area en la CPU 
addi $sp, $sp, 4		# restauramos el stack
jr $ra				# volvemos

