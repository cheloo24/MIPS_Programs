
addi $a0, $zero, 10
jal fib # Voy a fib con n = 10
j fin


# Calcula el fibonacci de n
# Entrada:
#   $a0 = el n a calcular
# Salida:
#   $v0 = fib(n)
#
# fib(0) = 0
# fib(1) = 1
# fib(n) = fib(n - 1) + fib(n - 2)
#
fib:
addi $t1, $zero, 1
beq $a0, $zero, BASE0   # f(0) = 0
beq $a0, $t1, BASE1     # f(1) = 1
addi $sp, $sp, -8       # Movemos el stack para 2 espacios
sw $a0, 0($sp)          # guardamos n
sw $ra, 4($sp)          # guaradmos $ra
addi $a0, $a0, -1       # n - 1
jal fib                 # fib(n - 1)
lw $a0, 0($sp)          # restauramos n
sw $v0, 0($sp)          # guardamos $v0 en donde estaba n antes
addi $a0, $a0, -2       # n - 2
jal fib                 # fib(n - 2)
lw $t0, 0($sp)          # cargamos $v0 de f(n - 1)
add $v0, $t0, $v0       # fib(n - 1) + fib(n - 2) 
lw $ra, 4($sp)          # restaruamos $ra
addi $sp, $sp, 8        # restauramos el stack
jr $ra                  # Volvemos a donde nos llamaron




BASE0:
BASE1:
add $v0, $zero, $a0     # return n;
jr $ra






fin:
