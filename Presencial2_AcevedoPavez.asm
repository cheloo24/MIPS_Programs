.data
L: 1,2,3,0
L2: 0,0,0,0

.text
la $a0,Doble
la $a1, L
la $a2, L2
jal map
j fin

map:
addi $sp,$sp,-8 #guarda 2 espacios
sw $a1,0($sp) #guarda en stack la direccion a1
sw $a0, 4($sp) # guarda en el stack la direccion de a0
la $t0, Doble #guardo la direccion a doble en t0
lw $a0,0($a1) #cargo el elemento de la lista en a0
beq $a0, $zero, condicion  #if(elemetoL == 0)
la $a1, ret #a1 con direcci�n al ret
jr $t0

ret:
la $t0,map #t0 con direcci�n a map
sw $v0, 0($a2) #guardo en mi lista 2 el resultado de v0
lw $a1, 0($sp) #cargo en a1 el a1 guardado en map
lw $a0, 4($sp) #cargo en a0 el a0 de map
addi $a1,$a1,4 #muevo la posicion en L
addi $a2,$a2,4 #muevo la posicion en L2
addi $sp, $sp,8 #bajo el stack
jr $t0 #salta a map 

condicion:
jr $ra #salta a fin

Doble: #
mul $v0,$a0,2 #v0 = valorLista1 *2
jr $a1


fin: