.data


archivo: .asciiz "prueba.txt"
buffer: .byte 0:100 #buffer de 100 bytes o 100 chars


.text

la $a0, archivo
addi $a1, $zero, 0  # flags: 0 leer, 1 escribir, 9 append
addi $a2, $zero, 0  # el modo no importa, es ignorado por MARS
addi $v0, $zero, 13 # la syscall 13 abre el archivo
syscall
add $t0, $zero, $v0 # copiamos el descriptor de archivo


add $a0, $zero, $t0
la $a1, buffer
addi $a2, $zero, 100
addi $v0, $zero, 14 # leemos desde el archivo
syscall

la $a0, buffer
addi $v0, $zero, 4
syscall
