.data

# mostramos hola cada vez que leemos un A
salida: .asciiz "hola"

.text


# Polling del teclado. Vamos preguntando cada 100ms si el estado del teclado es
# 'A' u otro valor
polling:
addi $a0, $zero, 100	# cada 100 ms
addi $v0, $zero, 32     # esperamos (syscall 32 sleep)
syscall			        # esperamos 100ms para no utilizar todo el procesador

addi $t0, $zero, 0xffff0004		# los caracteres del teclado se guardan en la dir de mem 0xffff0004
lw $t1, 0($t0)
sw $zero, 0($t0)		        # Seteamos a NULL (cero) el buffer para no leer datos duplicados
beq $t1, 65, mostrar # A mayuscula es 65 en ascii
j polling


# mostramos la cadena en salida y volvemos a hacer polling del teclado
mostrar: 
addi $v0, $zero, 4
la $a0, salida
syscall
j polling
