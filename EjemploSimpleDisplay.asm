.data

display: .word 0:131072


.text



la $a0, display             # direccion de memoria del display
addi $a3, $zero, 0x0033FF33 # vamos a mostrar una linea roja
addi $a1, $zero, 0          # en la direccion de memoria 0
addi $a2, $zero, 100        # hasta la direccion de memoria 100

jal dibujar_linea           # llamamos a dibujar linea



addi $v0, $zero, 10
syscall                     # terminamos la ejecucion del programa con la syscall 10


dibujar_linea:
# Dibuja una linea
# $a0 direccion de memoria del display
# $a1 inicio de la linea
# $a2 fin de la linea
# $a3 color de la linea

loop_dibujar_linea:                 # mientras
beq $a1, $a2, fin_dibujar_linea     # no lleguemos al final ($a2), seguimos dibujando
sw $a3, 0($a0)                      # copiamos el color en $a3 a la direccion de memoria del display
addi $a0, $a0, 4                    # nos movemos una palabra
addi $a1, $a1, 1                    # aumentamos el contador
j loop_dibujar_linea                # volvemos al "while"

fin_dibujar_linea:                  # terminamos la funcion
jr $ra                              # retornamos
