.data
L: 1,2,3,0
L2: 0,0,0,0

.text
la $a0,Doble
la $a1, L
la $a2, L2
jal map
j fin

map:
addi $sp,$sp,-8 #guarda 2 espacios
sw $a1,0($sp) #guarda en stack la direccion a1
sw $a0, 4($sp) # guarda en el stack la direccion de a0
addi $a1,$a1,4 #incrementa la posicion en la lista
la $t0, Doble
lw $a0,0($a1)
beq $a0, $zero, fin 
la $a1, ret
jr $t0

ret:
la $t0,map
sw $v0, 0($a2)
lw $a1, 0($sp)
lw $a0, 0($sp)
addi $a2,$a2,4
addi $sp, $sp,8
jr $t0


Doble:
mul $v0,$a0,2 #v0 = valorLista1 *2
jr $a1


fin: