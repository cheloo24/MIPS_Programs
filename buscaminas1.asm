.data
tablero: .word 
archivo: .asciiz "prueba.txt"
buffer: .byte 0:1024 #buffer de 1024
msg_error: .asciiz "ERROR :( "

.text
la $a0, archivo
addi $a1, $zero, 0  # flags: 0 leer, 1 escribir, 9 append
addi $a2, $zero, 0  # el modo no importa, es ignorado por MARS
addi $v0, $zero, 13 # la syscall 13 abre el archivo
syscall
add $t0, $zero, $v0 # copiamos el descriptor de archivo

LECTURA:
	add $a0, $zero, $t0 #asigno a a0 el descriptor del archivo
	la $a1, buffer #cargo el buffer
	addi $a2, $zero, 1023	#a2 con cuantos caracteres quiero leer
	addi $v0, $zero, 14 # leemos desde el archivo
	syscall
	la $v0, buffer #queda como salida todo lo leido
	
#
#PARA CONTAR FILAS NECESITO EL BUFFER
#RETORNA LA CANTIDAD DE FILAS Y EL BUFFER
#
CUENTA_FILAS:
	addi $a0, $v0, 0 # dejo como entrada el buffer con todo lo leido
	add $v1, $a0, $zero #salida el buffer 
	addi $t1,$zero,0 #contador de columnas
	while:
		lb $t0, 0($a0)
		addi $a0, $a0,1
		addi $t1, $t1, 1
		beq $t0 , 88, while
		beq $t0, 32, while
		addi $t1, $t1, -1
	add $v0, $t1, $zero #salida la cantidad de columnas
	
#
#PARA AGREGAR LOS VALORES AL ARREGLO NECESITO EL BUFFER Y EL ARREGLO VACIO(TABLERO)
#RETORNA EL ARREGLO LLENO(TABLERO)
#
AGREGA_ARREGLO:
		la $a0, tablero #primer parametro de entrada el arreglo
		add $a1, $v1, $zero #seg prametro de entrada el buffer
		add $a2, $v0, $zero #tercer parametro de entrada cantidad de filas
		addi $t0, $a1, 0 #copio el buffer
		addi $t1, $a0, 0 #copio el tablero
		ciclo:
			lb $t2, ($t0) #extrae el caracter del buffer
			addi $t0,$t0,1 #avanza al siguiente byte o caracter
			beq $t2, 88, si #si es una X va a si
			beq $t2, 32, no #si es un espacio va a no
			beq $t2, 3, final #si es fin de texto va al final
			beq $t2, 13, ciclo # si es un enter vuelve a ciclo
			j ciclo
			#j error #si no es ninguno de los casos hay error
		no:
			addi $t3, $zero, 0
			sw $t3,0($t1)
			addi $t1, $t1,4
			j ciclo
		si:
			addi $t3, $zero, -1
			sw $t3,0($t1)
			addi $t1, $t1,4
			j ciclo
		error:
			li $v0, 4
			la $a0, msg_error
			syscall
		final:
			
				
	
		
		
	
fin:

	
