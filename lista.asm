# Implementa una lista enlazada
#
# Cada nodo de la lista es de 8 bytes (4 para el número y 4 para el puntero al
# siguiente nodo)
#
# Creado por Felipe Garay
#


# Reservamos mem para un nodo
addi $a0, $zero, 8 	# 8 bytes
addi $v0, $zero, 9	# syscall 9
syscall
add $t8, $v0, $zero # Este nodo esta en $t8

addi $t1, $zero, 1
sw $t1, 0($t8)      # El elemento que guarda este nodo es 1



# Reservamos mem para otro nodo
addi $a0, $zero, 8 	# 8 bytes
addi $v0, $zero, 9	# syscall 9
syscall
add $t9, $v0, $zero # El nodo lo guardamos en  $t9

addi $t1, $zero, 2
sw $t1, 0($t9)      # Este nodo guarda 2
sw $t9, 4($t8)      # El nodo anterior en $t8 apunta a este nuevo nodo en $t9
add $t1, $zero, $zero 
sw $t1, 4($t9)      # Y el nuevo nodo apunta a 0 o NULL


add $a0, $zero, $t8     # Le damos como argumento a la función el primer nodo
jal funcion             # Llamamos a la funcion 

addi $v0, $zero, 10 
syscall                 # llamamos a la syscall 10 para terminar el programa


# Muestra los elementos de una lista enlazada
#
# $a0 => Dirección del nodo en que parte la lista enlazada
#
funcion:
lw $t0, 4($a0)              # Cargamos el puntero de este nodo
beq $t0, $zero, FIN         # ¿Es el puntero NULL?, si lo es vamos a FIN
add $t1, $zero, $a0         # copiamos a $t1 el nodo
lw $a0, 0($a0)              # cargamos el elemento en el nodo actual a $a0 para imprimirlo
addi $v0, $zero, 1          # la syscall 1 imprime enteros
syscall     
addi $sp, $sp, -8           # necesitamos guardar dos registros en el stack
sw $t1, 0($sp)              # $t1 tenia el nodo actual
sw $ra, 4($sp)              # guardamos ra para para recursiva
lw $a0, 4($t1)              # cargamos la direccion de memoria que apunta el siguiente nodo
jal funcion         
lw $ra, 4($sp)              # cuando termina la funcion recuperamos ra
lw $a0, 0($sp)              # y recuperamos el nodo original
addi $sp, $sp, 8            # restauramos el stack
jr $ra                      # volvemos

FIN:                        # caso en que el siguiente nodo sea null
lw $a0, 0($a0)              # igual tiene un elemento asi que lo cargamos
addi $v0, $zero, 1          
syscall                     # llamamos a la syscall 1 para imprimir el elemento
jr $ra                      # retornamos
