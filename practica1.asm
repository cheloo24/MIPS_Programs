#t0 = a; t1= b; t2 = c; t3= d; t4=E
addi $t0,$zero,0 #t0 = 10
addi $t1,$zero,1 #t1 = 20
addi $t2,$zero,100 #t2 = 20
addi $t3,$zero,40 #t3 = 20
#add $t5,$t0,$t1 #sumo t0 con t1 y guardo en t5
#add $t6,$t2,$t3 #sump t2 con t3 y guardo en t6
#mul $t4,$t5,$t6 #multiplico t5 con t6 y guardo en t4 

#--------------------------------------------------------
#bne $t0,$t2,if # si t0 es distinto a t2 salta a if
#beq $t0,$t2,else # si t0 es igual a t2 salta a else
#if: # etiqueta (label)
#add $t4,$t0,$t1
#j fin # salta a fin
#else: #etiqueta (label)
#add $t4,$t2,$t3
#j fin # salta a fin
#fin: #etiqueta (label)

#---------------------------------------------------------

beq $t0,$t2,fin
while:
	addi $t0,$t0,1
	mul $t1,$t1,2
	bne $t0,$t2,while
fin:

